{* Module Code *}
<div id="content" class="content" style="visibility: visible;">
    <div class="moduleTitle">
        <h2 class="module-title-text">Care Management </h2>
    </div>

    <div class="clear"></div>
    <div class="row" style="height: 500px">
        <div class="col-xs-6" style="height: 50%; overflow: none">
            <div class="moduleTitle"><h3 class="module-title-text">Calling</h3></div>
            <div class="row">
                <input type="hidden" id="number_to_dial" value="" />
                <div class="col-xs-8" id="call_to_text"></div>
                <div class="col-xs-4">
                    <input class="button" type="button" id="call_button" value="Call"/>
                </div>
            </div>
        </div>
        <div class="col-xs-6" style="height: 50%; overflow: none">
            <div class="moduleTitle"><h3 class="module-title-text">Patients</h3></div>
            <div class="list-view-rounded-corners" style="height:100%; overflow:auto">
                <table class="list view table-responsive" cellspacing="0" cellpadding="0">
                    <thead>      
                        <tr>
                            <th class="td_alt quick_view_links">#</th>                    
                            <th scope="col" data-toggle="true" class=""><div>Name</div></th>
                            <th scope="col" data-toggle="true" class=""><div>Mobile Number</div></th>
                            <th scope="col" data-toggle="true" class=""><div>Diagnosis</div></th>
                        </tr>
                    </thead>
                    <tbody id="patientData">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
{literal}
    $(document).ready(function(){
        $(document).on("click", ".contactRow", function() {
            $('#call_to_text').html('Click Call to dial ' + $(this).data('name') + ' on ' + $(this).data('mobile'));
            $('#number_to_dial').val($(this).data('mobile'));
        })
        $(document).on("click", "#call_button", function () {
            window.open('tel: ' + $('#number_to_dial').val());
        })
    });

    $.ajax({
        url: "http://localhost/epic_dev/etl_rest/public/patients",
        //dataType: "application/json",
        success: function(data,status) {
            addDataToGrid(data.patients);
        },
        error: function (xhr, status, err) {
            console.log(xhr, status, err);
        }
    });

    function addDataToGrid(data) {
        var patientHtml = "";
        $.each(data, function (index, patient) {
            var className = (index % 2 == 0) ? "oddListRowS1" : "evenListRowS1";
            patientHtml+= '<tr class="' + className + ' contactRow" data-mobile="' + patient.HomePhone + '" data-name="' + patient.FirstName + '" style="cursor: pointer">';
            patientHtml+= '   <td scope="row" type="serial" field="serial" valign="top" align="left">' + (index+1) + '</td>';
            patientHtml+= '   <td scope="row" type="name" field="name" valign="top" align="left">' + patient.FirstName + ' ' + patient.LastName + '</td>';
            patientHtml+= '   <td scope="row" type="phone_mobile" field="phone_mobile" valign="top" align="left">' + patient.HomePhone + '</td>';
            patientHtml+= '   <td scope="row" type="title" field="title" valign="top" align="left">' + patient.Diagnoses + '</td>';
            patientHtml+= '</tr>';
        });
        $('#patientData').html(patientHtml);
    }
{/literal}
</script>