<?php
//Module Code
require_once 'include/MVC/View/SugarView.php';

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class EPMD_BarCodeViewList extends SugarView
{
    public $ss;
    public function preDisplay() {
        $this->ss = new Sugar_Smarty();
    }
    
    public function display() {
        parent::display();
        $this->smarty = new Sugar_Smarty();
        $this->smarty->display(__DIR__.'/../tpls/view.generate.tpl');
    }
}
