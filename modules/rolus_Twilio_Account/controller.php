<?php

class rolus_Twilio_AccountController extends SugarController
{
	/*
	*	this will save the default country code value in the Config database table to retrieve later on through Administrtion Object
	*/
	function action_saveCountry(){

		require_once('modules/Administration/Administration.php');
		$admin = new Administration();

		$admin->saveSetting("MySettings", "twilio_country_code",$_REQUEST['twilio_country_code']); //$admin->saveSetting("category", "name", "value");

		$cc = $_REQUEST['twilio_country_code'];

		$twilio_country_abbreviation = array (
						'+1' => 'US',
						'+503' => 'SV',
						'+52' => 'MX',
						'+1809' => 'DO',
						'+1829' => 'DO',
						'+1849' => 'DO',
						'+51' => 'PE',
						'+1787' => 'PR',
						'+1939' => 'PR',
						'+43'	=> 'AT',
						'+32' => 'BE',
						'+359' => 'BG',
						'+420' => 'CZ',
						'+45' => 'DK',
						'+372' => 'EE',
						'+358' => 'FI',
						'+33' => 'FR',
						'+49' => 'DE',
						'+30' => 'GR',
						'+353' => 'IE',
						'+39' => 'IT',
						'+371' => 'LV',
						'+370' => 'LT',
						'+352' => 'LU',
						'+356' => 'MT',
						'+31' => 'NL',
						'+48' => 'PL',
						'+351' => 'PT',
						'+40' => 'RO',
						'+421' => 'SK',
						'+34' => 'ES',
						'+46' => 'SE',
						'+41' => 'CH',
						'+44' => 'GB',
						'+973' => 'BH',
						'+86' => 'CN',
						'+357' => 'CY',
						'+852' => 'HK',
						'+91' => 'IN',
						'+972' => 'IL',
						'+81' => 'JP',
						'+27' => 'ZA',
						'+55' => 'BR',
						'+61' => 'AU',
						'+64' => 'NZ',
		);

		$ca = $twilio_country_abbreviation[$cc];

		$admin->saveSetting("MySettings", "twilio_country_abbreviation", $ca);

		//SugarApplication::redirect("index.php?module=Administration&action=index");  //using Sugar Redirect method
		SugarApplication::redirect("index.php?module=rolus_Twilio_Account");
	}

	/*
	*	this will save the password in encoded form for security purposes
	*/
   /* function pre_save()
    {
		parent::pre_save();
		if ($this->bean->pass == '**************************')
		{
			$this->bean->pass = $this->bean->fetched_row['pass'];
		}
		$this->bean->pass = blowfishEncode('TwilioAccount',$this->bean->pass);
    }*/

}
?>
