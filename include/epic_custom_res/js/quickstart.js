﻿$(function () {
  log('Connecting call agent...');
  $.getJSON('https://crm.epicpc.com/api/call/capability_token')
    .done(function (data) {
      // log('Got a token.');
      console.log('Token: ' + data.token);

      // Setup Twilio.Device
      Twilio.Device.setup(data.token);

      Twilio.Device.ready(function (device) {
        log('Calling agent Ready!');
        // document.getElementById('call-controls').style.display = 'block';
      });

      Twilio.Device.error(function (error) {
        log('Calling agent connection fails. Error: ' + error.message);
      });

      Twilio.Device.connect(function (conn) {
        log('Successfully established call!');
        // document.getElementById('button-call').style.display = 'none';
        // document.getElementById('button-hangup').style.display = 'inline';
      });

      Twilio.Device.disconnect(function (conn) {
        log('Call ended.');
        // document.getElementById('button-call').style.display = 'inline';
        // document.getElementById('button-hangup').style.display = 'none';
      });

      var incoming_conn;
      
      Twilio.Device.incoming(function (conn) {
        log('Incoming connection from ' + conn.parameters.From);
        var archEnemyPhoneNumber = '+12099517118';

        if (conn.parameters.From === archEnemyPhoneNumber) {
          conn.reject();
          log('Incoming call was on block list. Rejected call.');
        } else {
          getContactsByNumber('2485554000');
          incoming_conn = conn;
        }
      });

      $("#button-answer").click(function() {
        // accept the incoming connection and start two-way audio
        incoming_conn.accept();
      });

      setClientNameUI(data.identity);
    })
    .fail(function () {
      log('Could not get a token from server!');
    });

  // Bind button to make call
  document.getElementById('button-call').onclick = function () {
    // get the phone number to connect the call to
    var params = {
      To: document.getElementById('pmobile').value
    };

    console.log('Calling ' + params.To + '...');
    Twilio.Device.connect(params);
  };

  // Bind button to hangup call
  document.getElementById('button-hangup').onclick = function () {
    log('Hanging up...');
    Twilio.Device.disconnectAll();
  };

});

// Activity log
function log(message) {
  var logDiv = document.getElementById('log');
  logDiv.innerHTML += '<p>&gt;&nbsp;' + message + '</p>';
  logDiv.scrollTop = logDiv.scrollHeight;
}

// Set the client name in the UI
function setClientNameUI(clientName) {
  var div = document.getElementById('client-name');
  div.innerHTML = 'Your client name: <strong>' + clientName +
    '</strong>';
}
var contacts_data = [];
//Contact lookup by number:
function getContactsByNumber(number) {
  $.ajax({
    url: "https://crm.epicpc.com/api/patients/lookup_by_number",
    method: "GET",
    data: {
        number: number
    },
    dataType: "json",
    success: function(data,status) {
        console.log(data);
        contacts_data = data;
        showContactsList(data);
    },
    error: function (xhr, status, err) {
        console.log(xhr, status, err);
    }
  });
}

function showContactsList(data) {
  var html = 'No contact found';
  var first_key = null;
  for(key in data) {
    if(first_key === null) {
      first_key = key;
    }
    html+= '<div class="radio">';
    html+= '  <label><input type="radio" name="optionsRadios" id="contact_radio_' + data[key]['ID'] + '" class="contact_radio_button" value="'+ data[key]['ID'] +'">'+data[key]['lastname'] + ' ' + data[key]['firstname'] +'</label>';
    html+= '</div>';
  }
  $('#contacts_list').html(html);
  $('#contact_radio_'+first_key).prop("checked", 'checked');
  displayDemographicData(data[first_key]);
}

$(document).on('change', '.contact_radio_button', function() {
  displayDemographicData(contacts_data[$(this).val()]);
})

function displayDemographicData(data) {
  $('#pfname').val(data.firstname);
  $('#plname').val(data.lastname);
  $('#pdob').val(data.DOB);
  $('#pstreet').val(data.AddressLine1);
  $('#pcity').val(data.City);
  $('#pstate').val(data.State);
  $('#pzip').val(data.ZipCode);
  $('#pmobile').val(data.Cellphone);
  $('#phonenum').val(data.PhoneNumber);
}
