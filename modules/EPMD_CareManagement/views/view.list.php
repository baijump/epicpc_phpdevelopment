<?php
//Module Code
require_once 'include/MVC/View/SugarView.php';

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class EPMD_CareManagementViewList extends SugarView
{
    public $ss;
    public function preDisplay() {
        $this->ss = new Sugar_Smarty();
    }
    
    public function display() {
        echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.1/css/bootstrap-slider.min.css">';
        parent::display();
        $this->smarty = new Sugar_Smarty();
        $this->smarty->display(__DIR__.'/../tpls/cm.dashboard.tpl');
    }
}
