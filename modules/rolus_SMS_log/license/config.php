<?php
/**
*Licence validation through SuiteOutfitters
*
**/
$outfitters_config = array(
    'name' => 'SampleLicenseAddon',
    'shortname' => 'samplelicenseaddon',
    'public_key' => '49ad23bff7c9cb22652196f8c7b7889a',
    'api_url' => 'https://store.suitecrm.com/api/v1',
    'validate_users' => false,
    'manage_licensed_users' => false,
    'validation_frequency' => 'weekly',
    'continue_url' => '',
);
