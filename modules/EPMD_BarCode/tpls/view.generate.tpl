<style>
{literal}
    .top {
        margin: 20px 0px;
    }

    .black-border {
        border: 1px solid black
    }

    .profile-userpic img {
        display: inline;
        float: none;
        margin: 0 auto;
        width: 50%;
        height: 50%;
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important;
    }

    .padding-10 {
        padding: 10px;
    }

    .row-visible {
        min-height: 53px;
    }

    .top-10 {
        margin-top: 10px;
    }

    #ex1Slider .slider-selection {
        background: #BABABA;
    }

    .slider {
        display: inline-block;
        vertical-align: top;
        position: relative;
    }

    .slider.slider-horizontal {
        width: 90px;
    }

    .btn-min-width {
        min-width: 200px;
    }

{/literal}
</style>
<input type="hidden" id="pid" value="" />
<div class="row top">
    <div class="col-md-3">
        <div class="form-group ui-widget">
            <input id="patients_dd" class="form-control" type="text" placeholder="Patient Search"/>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="form-group">
                <input class="form-control" readonly id="pbarcode" type="text" placeholder="Bar Code"/>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-6">
                <button id="genBarCode" class="btn btn-default btn-block">Generate Bar Code</button>
            </div>
        </div>
    </div>
</div>
<div class="row top black-border">
    <div class="container-fluid">
        <div class="row">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <label>Demographic Preferences</label>
                    </div>
                </div>
                <div class="row padding-10">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pfname" type="text" placeholder="First Name"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="plname" type="text" placeholder="Last Name"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pdob" type="text" placeholder="DOB"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pstreet" type="text" placeholder="Street"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pcity" type="text" placeholder="City"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pstate" type="text" placeholder="State"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pzip" type="text" placeholder="Zip Code"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pmobile" type="text" placeholder="Mobile #"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="phomenum" type="text" placeholder="Home #"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pemail" type="text" placeholder="Email*"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pemergencynum" type="text" placeholder="Emergency Contact"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pprimaryins" type="text" placeholder="Primary Ins"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="psecondaryins" type="text" placeholder="Secondary Ins"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pprefloc" type="text" placeholder="Preferred Location"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pprefpract" type="text"
                                            placeholder="Preferred Practitioner"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="ppreftslot" type="text" placeholder="Preferred Time Slot"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="pprefcontactmethod" type="text"
                                            placeholder="Preferred Contact Method"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="passignedagent" type="text" placeholder="Assisted Agent"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select class="form-control" type="text" placeholder="Non-EPIC Preferences">
                                        <option value="1">1</option>
                                        <option value="2">3</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
{literal}
    $(function() {
        function display( data ) {
            $('#pid').val(data.ID);
            $('#pfname').val(data.FirstName);
            $('#plname').val(data.LastName);
            $('#pdob').val(data.DOB);
            $('#pstreet').val(data.Address1);
            $('#pcity').val(data.City);
            $('#pstate').val(data.State);
            $('#pzip').val(data.ZipCode);
            $('#pmobile').val(data.HomePhone);
            $('#phomenum').val(data.HomePhone);
            $('#pbarcode').val(data.barcode);
        }
    
        $("#patients_dd").autocomplete({
            source: "https://crm.epicpc.com/api/patients/search",
            minLength: 3,
            select: function( event, ui ) {
                display(ui.item);
            }
        });

        $(document).on("click", "#genBarCode", function () {
            $.ajax({
                url: "https://crm.epicpc.com/api/patients/generate_bar_code",
                method: "POST",
                data: {
                    patientID: $("#pid").val()
                },
                dataType: "json",
                success: function(data,status) {
                    $('#pbarcode').val(data.barcode);
                },
                error: function (xhr, status, err) {
                    console.log(xhr, status, err);
                }
            });
        })
    });
{/literal}
    
</script>