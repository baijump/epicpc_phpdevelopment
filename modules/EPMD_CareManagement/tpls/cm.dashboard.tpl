<script src="include/epic_custom_res/js/twilio.min.js"></script>
<script src="include/epic_custom_res/js/slider.min.js"></script>
<script src="include/epic_custom_res/js/quickstart.js"></script>

<style>
{literal}
    .top {
        margin: 20px 0px;
    }

    .black-border {
        border: 1px solid black
    }

    .profile-userpic img {
        display: inline;
        float: none;
        margin: 0 auto;
        width: 50%;
        height: 50%;
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important;
    }

    .padding-10 {
        padding: 10px;
    }

    .row-visible {
        min-height: 53px;
    }

    .top-10 {
        margin-top: 10px;
    }

    #ex1Slider .slider-selection {
        background: #BABABA;
    }

    .slider {
        display: inline-block;
        vertical-align: top;
        position: relative;
    }

    .slider.slider-horizontal {
        width: 90px;
    }

    .btn-min-width {
        min-width: 200px;
    }

    ul.ui-autocomplete { z-index: 1100; }

{/literal}
</style>

<!-- Search Patient Modal -->
<div class="modal fade" id="searchPatientModal" tabindex="-1" role="dialog" aria-labelledby="searchPatientModalLabel" aria-hidden="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="searchPatientModalLabel">Search Patient</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group ui-widget">
            <input id="patients_dd" class="form-control" type="text" placeholder="Patient Search"/>
            <div id="patient-search-results" style="display:none;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="psrfname" type="text" placeholder="First Name"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="psrlname" type="text" placeholder="Last Name"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="psrdob" type="text" placeholder="DOB"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="psrmobile" type="text" placeholder="Mobile"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Edit Patient Modal -->
<div class="modal fade" id="editPatientModal" tabindex="-1" role="dialog" aria-labelledby="editPatientModalLabel" aria-hidden="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editPatientModalLabel">Edit Patient</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group ui-widget">
            <div id="patient-edit-form">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpfid" type="hidden" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpfname" type="text" placeholder="First Name*"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpmname" type="text" placeholder="Middle Name"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editplname" type="text" placeholder="Last Name*"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpdob" type="text" placeholder="DOB"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpmobile" type="text" placeholder="Mobile"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpsuffix" type="text" placeholder="Suffix"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpgender" type="text" placeholder="Gender*" required/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpstreet1" type="text" placeholder="Street 1"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpstreet2" type="text" placeholder="Street 2"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpcity" type="text" placeholder="City"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpstate" type="text" placeholder="State"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpzip" type="text" placeholder="Zip Code"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" id="editpemail" type="text" placeholder="Email Address"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="update_patient" class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<!-- Twilio starts here. -->
<link rel="stylesheet" href="include/epic_custom_res/css/site.css">

{* <div id="controls">
<div id="info" style="display: none !important;">
    <p class="instructions">Twilio Client</p>
    <div id="client-name"></div>
</div>
 <div id="call-controls" style="display: none !important;">
    <p class="instructions">Make a Call:</p>
    <input id="phone-number" type="text" placeholder="Enter a phone # or client name" />
    <button id="button-call">Call</button>
    <button id="button-hangup">Hangup</button>
</div> 
<div id="log"></div>
</div> *}
<div style="display:none !important;" class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Patient Data</label>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" id="pfname1" type="text" placeholder="First Name"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" id="plname1" type="text" placeholder="Last Name"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" id="pdob1" type="text" placeholder="DOB"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" id="pstreet1" type="text" placeholder="Street"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" id="pcity1" type="text" placeholder="City"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" id="pstate1" type="text" placeholder="State"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" id="pzip1" type="text" placeholder="Zip Code"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" id="pmobile1" type="text" placeholder="Mobile #"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" id="phomenum1" type="text" placeholder="Home #"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" id="pemail1" type="text" placeholder="Email*"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Emergency Contact"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Primary Ins"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Secondary Ins"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Preferred Location"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text"
                                                placeholder="Preferred Practitioner"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Preferred Time Slot"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text"
                                                placeholder="Preferred Contact Method"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Assisted Agent"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control" type="text" placeholder="Non-EPIC Preferences">
                                            <option value="1">1</option>
                                            <option value="2">3</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
<!-- Twilio ends here. -->
<br>
<div class="row top">
    <div class="col-md-9">
        <div class="row">
            <!-- Patient Search button trigger modal -->
            <div class="col-md-4">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#searchPatientModal">
                Search Patient
                </button>
            </div>
            <div class="col-md-4">
                <button class="btn btn-default btn-block">Inbound Phone Call Encounter</button>
            </div>
            <div class="col-md-4">
                <button class="btn btn-default" data-toggle="modal" data-target="#editPatientModal">Edit Patient <i class="glyphicon glyphicon-pencil"></i></button>
                <button class="btn btn-default">Add New <i class="glyphicon glyphicon-user"></i></button>
            </div>
        </div>
    </div>
</div>
<div class="row top black-border">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <label id="contact_info">John Smith, 12-12-1982</label>
            </div>
        </div>
        <div class="row padding-10">
            <div class="col-lg-3 col-md-4 col-sm-5">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group btn-group-justified" role="group" aria-label="Com Group">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary">Phone</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-default">IM</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-default">Chat</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-default">Video</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    {* <div class="col-md-12">
                        <div class="profile-userpic" style="text-align: center;">
                            <img class="img-responsive" src="https://app.immivertex.com/assets/img/user.ico" alt="User Image"/>
                        </div>

                    </div> *}
                    <div class="col-md-12" style="height: 100px; overflow:auto" id="log"></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group btn-group-justified" role="group" aria-label="Com Group">
                            <div class="btn-group" role="group">
                                <button type="button" id="button-answer" class="btn btn-primary">Answer</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-default">Hold</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-default">Transfer</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="button" id="button-hangup" class="btn btn-default">End</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row top-10">
                    <div class="col-md-6">
                        <!-- Split button -->
                        <div class="btn-group">
                            <button type="button" id="button-call" class="btn btn-default">Contact</button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 top-10">
                        <span style="font-size: 16px"><i class="glyphicon glyphicon-volume-up"></i></span>
                        <input id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="10"
                               data-slider-step="1" data-slider-value="14"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-8 col-sm-7">
                <div class="row black-border">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Demographic Preferences</label>
                            </div>
                        </div>
                        <div class="row padding-10">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pfname" type="text" placeholder="First Name"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="plname" type="text" placeholder="Last Name"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pdob" type="text" placeholder="DOB"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pstreet" type="text" placeholder="Street"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pcity" type="text" placeholder="City"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pstate" type="text" placeholder="State"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pzip" type="text" placeholder="Zip Code"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pmobile" type="text" placeholder="Mobile #"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="phonenum" type="text" placeholder="Home #"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pemail" type="text" placeholder="Email*"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pemergencynum" type="text" placeholder="Emergency Contact"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pprimaryins" type="text" placeholder="Primary Ins"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="psecondaryins" type="text" placeholder="Secondary Ins"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pprefloc" type="text" placeholder="Preferred Location"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pprefpract" type="text"
                                                   placeholder="Preferred Practitioner"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="ppreftslot" type="text" placeholder="Preferred Time Slot"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="pprefcontactmethod" type="text"
                                                   placeholder="Preferred Contact Method"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" id="passignedagent" type="text" placeholder="Assisted Agent"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select class="form-control" type="text" placeholder="Non-EPIC Preferences">
                                                <option value="1">1</option>
                                                <option value="2">3</option>
                                                <option value="3">3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row black-border">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Service Lines</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                Care Management (TOC, CDM)
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                Exercise Phys (Fall Risk, Obesity)
                                            </div>
                                        </div>
                                        <div class="row row-visible">
                                            <div class="col-md-12">
                                            </div>
                                        </div>
                                        <div class="row row-visible">
                                            <div class="col-md-12">
                                            </div>
                                        </div>
                                        <div class="row row-visible">
                                            <div class="col-md-12">
                                            </div>
                                        </div>
                                        <div class="row row-visible">
                                            <div class="col-md-12">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row top-10">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button class="btn btn-default btn-block">Edit / Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-5">
                <div class="row top black-border">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Related #'s</label>
                            </div>
                        </div>
                        <div class="row padding-10">
                            <div class="col-md-12" id="contacts_list">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1"
                                               checked>
                                        Mr. Jhon Smith
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Wife Smith (needs to be seen)
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                        Kid1 Smith
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios4" value="option4">
                                        Kid2 Smith
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios5" value="option5">
                                        Dad Smith
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">Associated Numbers</button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Cardiology xx-xx-xxx</a></li>
                                        <li><a href="#">Pulmonary xx-xx-xxx</a></li>
                                        <li><a href="#">Surgery xx-xx-xxx</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-8 col-sm-7">
                <div class="row top black-border">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Call Information</label>
                            </div>
                        </div>
                        <div class="row padding-10">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default">Reason's for Call</button>
                                            <button type="button" class="btn btn-default dropdown-toggle"
                                                    data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" placeholder="Notes"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="well well-sm text-center">Inbound Call</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="well well-sm text-center">Start Time</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="well well-sm text-center">End Time</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Open Gaps in Care
                                            </label>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1">
                                                    Option one
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="2">
                                                    Option two
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="3">
                                                    Option three
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Health Opportunities
                                            </label>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1">
                                                    Candidate 1
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="2">
                                                    Candidate 2
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="3">
                                                    Candidate 3
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row top black-border">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Task Management</label>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="form-group">
                                                <!-- Single button -->
                                                <div class="btn-group">
                                                    <button type="button"
                                                            class="btn btn-default btn-min-width dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        One-Click Task Creator
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Task Category</a></li>
                                                        <li><a href="#">Task Priority</a></li>
                                                        <li><a href="#">Task Assignment</a></li>
                                                        <li><a href="#">Task Forms</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="form-group">
                                                <!-- Single button -->
                                                <div class="btn-group">
                                                    <button type="button"
                                                            class="btn btn-default btn-min-width dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        One-Click Scheduler
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Option 1</a></li>
                                                        <li><a href="#">Option 2</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="form-group">
                                                <!-- Single button -->
                                                <div class="btn-group">
                                                    <button type="button"
                                                            class="btn btn-default btn-min-width dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        One-Click Med Request
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Option 1</a></li>
                                                        <li><a href="#">Option 2</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row top black-border">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <label>Care Management</label>
            </div>
        </div>
        <form class="form-inline">
            <div class="row padding-10">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="start-date">Start Date</label>
                        <input type="date" class="form-control" id="start-date">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="end-date">End Date</label>
                        <input type="date" class="form-control" id="end-date">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="care-manager">Assigned Care Manager</label>
                        <select class="form-control" type="text" placeholder="Assigned Care Manager" id="care-manager">
                            <option value="1">Manager 1</option>
                            <option value="2">Manager 2</option>
                            <option value="3">Manager 3</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <!--
        Accordian Starts from below-->
        <div class="row">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                               aria-expanded="true" aria-controls="collapseOne">
                                Active Conditions / Health Summary
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="headingOne">
                        <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                            squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                            single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                            beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                            lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                            probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Medications / Orders
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                            squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                            single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                            beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                            lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                            probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Goals / Care Plans
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingThree">
                        <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                            squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                            single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                            beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                            lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                            probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Engagement
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingFour">
                        <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                            squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                            single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                            beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                            lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                            probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFive">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Barriers to Care
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingFive">
                        <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                            squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                            single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                            beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                            lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                            probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSix">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                History / Encounters
                            </a>
                        </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                        <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                            squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                            single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                            beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                            lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                            probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSeven">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                Outcomes
                            </a>
                        </h4>
                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingSeven">
                        <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                            squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                            single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                            beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                            lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                            probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingEight">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                Notes
                            </a>
                        </h4>
                    </div>
                    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingEight">
                        <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                            squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                            single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                            beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                            lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                            probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Accordian Ends Above-->
        <div class="row pull-right padding-10">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary">Save and Send Change</button>
            </div>
        </div>
    </div>
</div>
<script>
{literal}
    $(function() {
        function display( data ) {
            $('#pfname').val(data.firstname);
            $('#plname').val(data.lastname);
            $('#pdob').val(data.dateofbirth);
            $('#pstreet').val(data.AddressLine1);
            $('#pcity').val(data.City);
            $('#pstate').val(data.State);
            $('#pzip').val(data.ZipCode);
            $('#pmobile').val(data.HomePhone);
            $('#phonenum').val(data.HomePhone);
        }
    
        function populateSearchResults(data) {
            console.log(data);
            $('#patient-search-results').css("display", "block");
            $('#psrfname').val(data.firstname);
            $('#psrlname').val(data.lastname);
            $('#psrdob').val(data.dateofbirth);
            $('#psrmobile').val(data.HomePhone);
        }

        function populateEditFields(data) {
            $('#editpfname').val(data.firstname);
            $('#editplname').val(data.lastname);
            $('#editpdob').val(data.dateofbirth);
            $('#editpstreet').val(data.AddressLine1);
            $('#editpcity').val(data.City);
            $('#editpstate').val(data.State);
            $('#editpzip').val(data.ZipCode);
            $('#editpmobile').val(data.HomePhone);
            $('#editpgender').val(data.gender);
        }
        $("#patients_dd").autocomplete({
            source: "https://crm.epicpc.com/api/patients/search",
            minLength: 3,
            select: function( event, ui ) {
                populateSearchResults(ui.item);
                display(ui.item);
                populateEditFields(ui.item);
                $('#editpfid').val(ui.item.ID);
                //call api to update demographic data
                $.ajax({
                url: "https://crm.epicpc.com/api/patients//updateETL",
                method: "POST",
                data: {
                    id: ui.item.ID,
                    firstName: ui.item.firstname,
                    lastName: ui.item.lastname,
                    middleName: ui.item.middlename,
                    suffix: ui.item.suffix,
                    street1: ui.item.AddressLine1,
                    street2: ui.item.AddressLine2,
                    city: ui.item.City,
                    state: ui.item.State,
                    zipCode: ui.item.ZipCode,
                    mobile: ui.item.HomePhone,
                    dob: ui.item.dateofbirth,
                    gender: ui.item.gender
                },
                dataType: "application/json",
                success: function(data,status) {
                    console.log(data);
                },
                error: function (xhr, status, err) {
                    console.log(xhr, status, err);
                }
            });
            }
        });
    
        $(document).on("click", "#call_button", function() {
            $.ajax({
                url: "https://crm.epicpc.com/api/patients/call",
                method: "GET",
                data: {
                    number: $("#pmobile").val()
                },
                dataType: "application/json",
                success: function(data,status) {
                    console.log(data);
                },
                error: function (xhr, status, err) {
                    console.log(xhr, status, err);
                }
            });
        })

        $(document).on("click", "#update_patient", function() {
            $.ajax({
                url: "https://crm.epicpc.com/api/patients/update",
                method: "POST",
                data: {
                    id: $("#editpfid").val(),
                    firstName: $("#editpfname").val(),
                    lastName: $("#editplname").val(),
                    middleName: $("#editpmname").val(),
                    suffix: $("#editpsuffix").val(),
                    street1: $("#editpstreet1").val(),
                    street2: $("#editpstreet2").val(),
                    city: $("#editpcity").val(),
                    state: $("#editpstate").val(),
                    zipCode: $("#editpzip").val(),
                    mobile: $("#editpmobile").val(),
                    dob: $("#editpdob").val(),
                    gender: $('#editpgender').val()
                },
                dataType: "application/json",
                success: function(data,status) {
                    console.log(data);
                },
                error: function (xhr, status, err) {
                    console.log(xhr, status, err);
                }
            });
        })
    });
    var slider = new Slider('#ex1', {
        formatter: function (value) {
            return 'Current value: ' + value;
        }
    });
{/literal}
    
</script>
